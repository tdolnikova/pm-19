
package com.dolnikova.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.dolnikova.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CheckPassword_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "checkPassword");
    private final static QName _CheckPasswordResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "checkPasswordResponse");
    private final static QName _FindAllByLoginUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findAllByLoginUser");
    private final static QName _FindAllByLoginUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findAllByLoginUserResponse");
    private final static QName _FindAllUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findAllUser");
    private final static QName _FindAllUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findAllUserResponse");
    private final static QName _FindOneByLoginUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findOneByLoginUser");
    private final static QName _FindOneByLoginUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findOneByLoginUserResponse");
    private final static QName _FindOneBySession_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findOneBySession");
    private final static QName _FindOneBySessionResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "findOneBySessionResponse");
    private final static QName _MergeUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "mergeUser");
    private final static QName _MergeUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "mergeUserResponse");
    private final static QName _PersistListUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "persistListUser");
    private final static QName _PersistListUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "persistListUserResponse");
    private final static QName _PersistUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "persistUser");
    private final static QName _PersistUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "persistUserResponse");
    private final static QName _RemoveAllUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "removeAllUser");
    private final static QName _RemoveAllUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "removeAllUserResponse");
    private final static QName _RemoveUser_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "removeUser");
    private final static QName _RemoveUserResponse_QNAME = new QName("http://endpoint.tm.dolnikova.com/", "removeUserResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.dolnikova.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckPassword }
     * 
     */
    public CheckPassword createCheckPassword() {
        return new CheckPassword();
    }

    /**
     * Create an instance of {@link CheckPasswordResponse }
     * 
     */
    public CheckPasswordResponse createCheckPasswordResponse() {
        return new CheckPasswordResponse();
    }

    /**
     * Create an instance of {@link FindAllByLoginUser }
     * 
     */
    public FindAllByLoginUser createFindAllByLoginUser() {
        return new FindAllByLoginUser();
    }

    /**
     * Create an instance of {@link FindAllByLoginUserResponse }
     * 
     */
    public FindAllByLoginUserResponse createFindAllByLoginUserResponse() {
        return new FindAllByLoginUserResponse();
    }

    /**
     * Create an instance of {@link FindAllUser }
     * 
     */
    public FindAllUser createFindAllUser() {
        return new FindAllUser();
    }

    /**
     * Create an instance of {@link FindAllUserResponse }
     * 
     */
    public FindAllUserResponse createFindAllUserResponse() {
        return new FindAllUserResponse();
    }

    /**
     * Create an instance of {@link FindOneByLoginUser }
     * 
     */
    public FindOneByLoginUser createFindOneByLoginUser() {
        return new FindOneByLoginUser();
    }

    /**
     * Create an instance of {@link FindOneByLoginUserResponse }
     * 
     */
    public FindOneByLoginUserResponse createFindOneByLoginUserResponse() {
        return new FindOneByLoginUserResponse();
    }

    /**
     * Create an instance of {@link FindOneBySession }
     * 
     */
    public FindOneBySession createFindOneBySession() {
        return new FindOneBySession();
    }

    /**
     * Create an instance of {@link FindOneBySessionResponse }
     * 
     */
    public FindOneBySessionResponse createFindOneBySessionResponse() {
        return new FindOneBySessionResponse();
    }

    /**
     * Create an instance of {@link MergeUser }
     * 
     */
    public MergeUser createMergeUser() {
        return new MergeUser();
    }

    /**
     * Create an instance of {@link MergeUserResponse }
     * 
     */
    public MergeUserResponse createMergeUserResponse() {
        return new MergeUserResponse();
    }

    /**
     * Create an instance of {@link PersistListUser }
     * 
     */
    public PersistListUser createPersistListUser() {
        return new PersistListUser();
    }

    /**
     * Create an instance of {@link PersistListUserResponse }
     * 
     */
    public PersistListUserResponse createPersistListUserResponse() {
        return new PersistListUserResponse();
    }

    /**
     * Create an instance of {@link PersistUser }
     * 
     */
    public PersistUser createPersistUser() {
        return new PersistUser();
    }

    /**
     * Create an instance of {@link PersistUserResponse }
     * 
     */
    public PersistUserResponse createPersistUserResponse() {
        return new PersistUserResponse();
    }

    /**
     * Create an instance of {@link RemoveAllUser }
     * 
     */
    public RemoveAllUser createRemoveAllUser() {
        return new RemoveAllUser();
    }

    /**
     * Create an instance of {@link RemoveAllUserResponse }
     * 
     */
    public RemoveAllUserResponse createRemoveAllUserResponse() {
        return new RemoveAllUserResponse();
    }

    /**
     * Create an instance of {@link RemoveUser }
     * 
     */
    public RemoveUser createRemoveUser() {
        return new RemoveUser();
    }

    /**
     * Create an instance of {@link RemoveUserResponse }
     * 
     */
    public RemoveUserResponse createRemoveUserResponse() {
        return new RemoveUserResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link AbstractDTO }
     * 
     */
    public AbstractDTO createAbstractDTO() {
        return new AbstractDTO();
    }

    /**
     * Create an instance of {@link UserDTO }
     * 
     */
    public UserDTO createUserDTO() {
        return new UserDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "checkPassword")
    public JAXBElement<CheckPassword> createCheckPassword(CheckPassword value) {
        return new JAXBElement<CheckPassword>(_CheckPassword_QNAME, CheckPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "checkPasswordResponse")
    public JAXBElement<CheckPasswordResponse> createCheckPasswordResponse(CheckPasswordResponse value) {
        return new JAXBElement<CheckPasswordResponse>(_CheckPasswordResponse_QNAME, CheckPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllByLoginUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findAllByLoginUser")
    public JAXBElement<FindAllByLoginUser> createFindAllByLoginUser(FindAllByLoginUser value) {
        return new JAXBElement<FindAllByLoginUser>(_FindAllByLoginUser_QNAME, FindAllByLoginUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllByLoginUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findAllByLoginUserResponse")
    public JAXBElement<FindAllByLoginUserResponse> createFindAllByLoginUserResponse(FindAllByLoginUserResponse value) {
        return new JAXBElement<FindAllByLoginUserResponse>(_FindAllByLoginUserResponse_QNAME, FindAllByLoginUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findAllUser")
    public JAXBElement<FindAllUser> createFindAllUser(FindAllUser value) {
        return new JAXBElement<FindAllUser>(_FindAllUser_QNAME, FindAllUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findAllUserResponse")
    public JAXBElement<FindAllUserResponse> createFindAllUserResponse(FindAllUserResponse value) {
        return new JAXBElement<FindAllUserResponse>(_FindAllUserResponse_QNAME, FindAllUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneByLoginUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findOneByLoginUser")
    public JAXBElement<FindOneByLoginUser> createFindOneByLoginUser(FindOneByLoginUser value) {
        return new JAXBElement<FindOneByLoginUser>(_FindOneByLoginUser_QNAME, FindOneByLoginUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneByLoginUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findOneByLoginUserResponse")
    public JAXBElement<FindOneByLoginUserResponse> createFindOneByLoginUserResponse(FindOneByLoginUserResponse value) {
        return new JAXBElement<FindOneByLoginUserResponse>(_FindOneByLoginUserResponse_QNAME, FindOneByLoginUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneBySession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findOneBySession")
    public JAXBElement<FindOneBySession> createFindOneBySession(FindOneBySession value) {
        return new JAXBElement<FindOneBySession>(_FindOneBySession_QNAME, FindOneBySession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneBySessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "findOneBySessionResponse")
    public JAXBElement<FindOneBySessionResponse> createFindOneBySessionResponse(FindOneBySessionResponse value) {
        return new JAXBElement<FindOneBySessionResponse>(_FindOneBySessionResponse_QNAME, FindOneBySessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "mergeUser")
    public JAXBElement<MergeUser> createMergeUser(MergeUser value) {
        return new JAXBElement<MergeUser>(_MergeUser_QNAME, MergeUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "mergeUserResponse")
    public JAXBElement<MergeUserResponse> createMergeUserResponse(MergeUserResponse value) {
        return new JAXBElement<MergeUserResponse>(_MergeUserResponse_QNAME, MergeUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistListUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "persistListUser")
    public JAXBElement<PersistListUser> createPersistListUser(PersistListUser value) {
        return new JAXBElement<PersistListUser>(_PersistListUser_QNAME, PersistListUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistListUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "persistListUserResponse")
    public JAXBElement<PersistListUserResponse> createPersistListUserResponse(PersistListUserResponse value) {
        return new JAXBElement<PersistListUserResponse>(_PersistListUserResponse_QNAME, PersistListUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "persistUser")
    public JAXBElement<PersistUser> createPersistUser(PersistUser value) {
        return new JAXBElement<PersistUser>(_PersistUser_QNAME, PersistUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "persistUserResponse")
    public JAXBElement<PersistUserResponse> createPersistUserResponse(PersistUserResponse value) {
        return new JAXBElement<PersistUserResponse>(_PersistUserResponse_QNAME, PersistUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "removeAllUser")
    public JAXBElement<RemoveAllUser> createRemoveAllUser(RemoveAllUser value) {
        return new JAXBElement<RemoveAllUser>(_RemoveAllUser_QNAME, RemoveAllUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "removeAllUserResponse")
    public JAXBElement<RemoveAllUserResponse> createRemoveAllUserResponse(RemoveAllUserResponse value) {
        return new JAXBElement<RemoveAllUserResponse>(_RemoveAllUserResponse_QNAME, RemoveAllUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "removeUser")
    public JAXBElement<RemoveUser> createRemoveUser(RemoveUser value) {
        return new JAXBElement<RemoveUser>(_RemoveUser_QNAME, RemoveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dolnikova.com/", name = "removeUserResponse")
    public JAXBElement<RemoveUserResponse> createRemoveUserResponse(RemoveUserResponse value) {
        return new JAXBElement<RemoveUserResponse>(_RemoveUserResponse_QNAME, RemoveUserResponse.class, null, value);
    }

}
