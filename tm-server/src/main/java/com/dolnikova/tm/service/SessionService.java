package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.ISessionService;
import com.dolnikova.tm.api.spring.SessionSpringRepository;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.util.SignatureUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

@Service
@Transactional
@NoArgsConstructor
public class SessionService extends AbstractService<Session> implements ISessionService {

    @Autowired
    private SessionSpringRepository sessionRepository;
    private final Logger LOGGER = Logger.getLogger(SessionService.class.getName());

    @Override
    public void createSession(@Nullable final User user) {
        LOGGER.info("[Создание сессии для пользователя]");
        if (user == null) return;
        @NotNull final Session session = new Session();
        session.setUser(user);
        @Nullable final String signature = SignatureUtil.sign(user.getPasswordHash(), "JAVA", 3);
        session.setSignature(signature);
        sessionRepository.save(session);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String userId, @Nullable final String id) {
        LOGGER.info("[Поиск сессии по id]");
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return sessionRepository.findSessionById(id);
    }

    @Nullable
    @Override
    public Session findOneByUserId(@Nullable final String userId) {
        LOGGER.info("[Поиск сессии по id пользователя]");
        if (userId == null || userId.isEmpty()) return null;
        return sessionRepository.findSessionByUserId(userId);
    }

    @Nullable
    public Session findOneBySignature(@Nullable final String signature) {
        LOGGER.info("[Поиск сессии по сигнатуре]");
        if (signature == null || signature.isEmpty()) return null;
        return sessionRepository.findSessionBySignature(signature);
    }

    @Override
    public @Nullable List<Session> findAllByUserId(@Nullable final String userId) {
        LOGGER.info("[Поиск всех сессий]");
        if (userId == null || userId.isEmpty()) return null;
        sessionRepository.findAllByUserId(userId);
        return null;
    }

    @Override
    public void persist(@Nullable final Session session) {
        LOGGER.info("[Вставка сессии]");
        if (session == null) return;
        sessionRepository.save(session);
    }

    @Override
    public void persistList(@Nullable Collection<Session> list) {
        if (list == null || list.isEmpty()) return;
        sessionRepository.saveAll(list);
    }

    @Override
    public void remove(@Nullable final Session session) {
        LOGGER.info("[Удаление сессии]");
        if (session == null) return;
        sessionRepository.delete(session);
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) {
        LOGGER.info("[Удаление всех проектов пользователя]");
        if (userId == null || userId.isEmpty()) return;
        sessionRepository.deleteAllByUserId(userId);
    }

}
