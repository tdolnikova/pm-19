package com.dolnikova.tm.api.spring;

import com.dolnikova.tm.entity.Session;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface SessionSpringRepository extends JpaRepository<Session, String>  {

    Session findSessionById(@Nullable String id);

    Session findSessionByUserId(@Nullable String userId);

    Session findSessionBySignature(@Nullable String signature);

    Collection<Session> findAllByUserId(@Nullable String userId);

    void deleteAllByUserId(@Nullable String userId);

}
