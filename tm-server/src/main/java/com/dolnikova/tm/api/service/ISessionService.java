package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public interface ISessionService extends IService<Session> {

    void createSession(User user);

    @Nullable
    Session findOneById(final @Nullable String ownerId, final @Nullable String id);

    @Nullable
    Session findOneByUserId(final @Nullable String userId);

    @Nullable
    Session findOneBySignature(@Nullable final String signature);

    @Nullable List<Session> findAllByUserId(@Nullable final String ownerId);

    @Override
    void persist(final @Nullable Session entity);

    void persistList(final @Nullable Collection<Session> list);

    @Override
    void remove(final @Nullable Session entity);

    void removeAllByUserId(final @Nullable String ownerId);
}
