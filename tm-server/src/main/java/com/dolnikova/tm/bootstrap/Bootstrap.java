package com.dolnikova.tm.bootstrap;


import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.endpoint.ProjectEndpoint;
import com.dolnikova.tm.endpoint.SessionEndpoint;
import com.dolnikova.tm.endpoint.TaskEndpoint;
import com.dolnikova.tm.endpoint.UserEndpoint;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;

import javax.xml.ws.Endpoint;
import java.util.Scanner;

@Getter
@Setter
@Component
@Scope("singleton")
@EnableJpaRepositories
public final class Bootstrap {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Autowired
    private UserEndpoint userEndpoint;

    @Autowired
    private SessionEndpoint sessionEndpoint;

    public void init() {
        Endpoint.publish(General.ADDRESS + "TaskEndpoint?WSDL", taskEndpoint);
        Endpoint.publish(General.ADDRESS + "ProjectEndpoint?WSDL", projectEndpoint);
        Endpoint.publish(General.ADDRESS + "UserEndpoint?WSDL", userEndpoint);
        Endpoint.publish(General.ADDRESS + "SessionEndpoint?WSDL", sessionEndpoint);

        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (!"exit".equals(input)) {
            try {
                input = scanner.nextLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        System.exit(0);
    }

}
