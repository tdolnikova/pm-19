package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    Session findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Session findOneByUserId(@NotNull final String userId);

    @Nullable
    Session findOneBySignature(@NotNull final String signature);

    @Override
    @Nullable List<Session> findAll(final @NotNull String userId);

    @Override
    void persist(final @NotNull Session entity);

    @Override
    void remove(final @NotNull Session entity);

    @Override
    void saveBin(final @NotNull List<Session> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @NotNull List<Session> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @NotNull List<Session> entities) throws Exception;

    @Override
    void saveJaxbJson(final @NotNull List<Session> entities) throws Exception;

    @Override
    void saveJaxbXml(final @NotNull List<Session> entities) throws Exception;

    @Nullable
    @Override
    List<Session> loadBin() throws Exception;

    @Nullable
    @Override
    List<Session> loadFasterxmlJson() throws Exception;

    @Nullable
    @Override
    List<Session> loadFasterxmlXml() throws Exception;

    @Nullable
    @Override
    List<Session> loadJaxbJson() throws Exception;

    @Nullable
    @Override
    List<Session> loadJaxbXml() throws Exception;
}
