package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.IService;
import com.dolnikova.tm.entity.AbstractEntity;
import com.dolnikova.tm.repository.AbstractRepository;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

}
