package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @Nullable
    List<E> findAll(@NotNull final String userId);

    void persist(@NotNull final E entity);

    void remove(@NotNull final E entity);

    void saveBin(@NotNull final List<E> entities) throws Exception;

    void saveFasterxmlJson(@NotNull final List<E> entities) throws Exception;

    void saveFasterxmlXml(@NotNull final List<E> entities) throws Exception;

    void saveJaxbJson(@NotNull final List<E> entities) throws Exception;

    void saveJaxbXml(@NotNull final List<E> entities) throws Exception;

    @Nullable
    List<E> loadBin() throws Exception;

    @Nullable
    List<E> loadFasterxmlJson() throws Exception;

    @Nullable
    List<E> loadFasterxmlXml() throws Exception;

    @Nullable
    List<E> loadJaxbJson() throws Exception;

    @Nullable
    List<E> loadJaxbXml() throws Exception;

}
