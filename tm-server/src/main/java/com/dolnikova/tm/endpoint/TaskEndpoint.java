package com.dolnikova.tm.endpoint;

import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.dto.SessionDTO;
import com.dolnikova.tm.dto.TaskDTO;
import com.dolnikova.tm.dto.UserDTO;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.service.ProjectService;
import com.dolnikova.tm.service.TaskService;
import com.dolnikova.tm.service.UserService;
import com.dolnikova.tm.util.DtoConversionUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
@WebService
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint {

    @Autowired
    private UserService userService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @WebMethod
    @Nullable
    public TaskDTO findOneByIdTask(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                                   @WebParam(name = "id") @Nullable String id) {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return null;
        return DtoConversionUtil.taskToDto(taskService.findOneById(session.getUser().getId(), id));
    }

    @WebMethod
    @Nullable
    public TaskDTO findOneByNameTask(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                                     @WebParam(name = "name") @Nullable String name) {
        if (sessionDTO == null) return null;
        @Nullable final String userId = sessionDTO.getUserId();
        @Nullable final User user = userService.findOneById(userId);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        return DtoConversionUtil.taskToDto(taskService.findOneByName(name, userId));
    }

    @WebMethod
    public @Nullable List<TaskDTO> findAllTask(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return null;
        @Nullable final Collection<Task> tasks = taskService.findAll(session.getUser().getId());
        System.out.println("2");
        if (tasks == null) return null;
        @Nullable final List<TaskDTO> list = new ArrayList<>();
        for (Task task : tasks) {
            list.add(DtoConversionUtil.taskToDto(task));
        }
        return list;
    }

    @WebMethod
    public @Nullable List<TaskDTO> findAllByNameTask(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                                                     @WebParam(name = "text") @Nullable String text) {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null || user == null) return null;
        @Nullable final Collection<Task> tasks = taskService.findAllByName(user.getId(), text);
        if (tasks == null) return null;
        @Nullable final List<TaskDTO> list = new ArrayList<>();
        for (Task task : tasks) {
            list.add(DtoConversionUtil.taskToDto(task));
        }
        return list;
    }

    @WebMethod
    public @Nullable List<TaskDTO> findAllByDescriptionTask(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                                                            @WebParam(name = "text") @Nullable String text) {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return null;
        if (user == null) return null;
        @Nullable final Collection<Task> tasks = taskService.findAllByDescription(user.getId(), text);
        if (tasks == null) return null;
        @Nullable final List<TaskDTO> list = new ArrayList<>();
        for (Task task : tasks) {
            list.add(DtoConversionUtil.taskToDto(task));
        }
        return list;
    }

    @WebMethod
    public void persistTask(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                            @WebParam(name = "entity") @Nullable TaskDTO taskDTO) {
        if (sessionDTO == null || taskDTO == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        if (user == null) return;
        System.out.println("user_id: " + taskDTO.getUserId() + "\nid: " + taskDTO.getProjectId());
        @Nullable final Project project = projectService.findOneById(taskDTO.getUserId(), taskDTO.getProjectId());
        if (project == null) {
            System.out.println("No projects found");
            return;
        }
        System.out.println("Project id: " + project.getId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return;

        taskService.persist(DtoConversionUtil.dtoToTask(taskDTO, project, user));
    }

    @WebMethod
    public void persistListTask(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                                @WebParam(name = "list") @Nullable List<TaskDTO> list) {
        if (sessionDTO == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null || list == null) return;
        @Nullable final List<Task> taskList = new ArrayList<>();
        for (TaskDTO taskDTO : list) {
            @Nullable final Project project = projectService.findOneById(taskDTO.getUserId(), taskDTO.getProjectId());
            taskList.add(DtoConversionUtil.dtoToTask(taskDTO, project, user));
        }
        taskService.persistList(taskList);
    }

    @WebMethod
    public void mergeTask(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                          @WebParam(name = "newData") @Nullable String newData,
                          @WebParam(name = "entityToMerge") @Nullable TaskDTO taskDTO,
                          @WebParam(name = "dataType") @Nullable DataType dataType) {
        if (sessionDTO == null || taskDTO == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        if (user == null) return;
        @Nullable final Project project = projectService.findOneById(taskDTO.getUserId(), taskDTO.getProjectId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return;
        @Nullable final Task task = DtoConversionUtil.dtoToTask(taskDTO, project, user);
        if (task == null) return;
        taskService.merge(newData, task, dataType);
    }

    @WebMethod
    public void removeTask(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                           @WebParam(name = "entity") @Nullable TaskDTO taskDTO) {
        if (sessionDTO == null || taskDTO == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        if (user == null) return;
        @Nullable final Project project = projectService.findOneById(taskDTO.getUserId(), taskDTO.getProjectId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return;
        Task task = DtoConversionUtil.dtoToTask(taskDTO, project, user);
        taskService.remove(task);
    }

    @WebMethod
    public void removeAllTask(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null || user == null) return;
        taskService.removeAll(user.getId());
    }

    /*@WebMethod
    public boolean saveBinTask(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "tasks") @Nullable List<TaskDTO> entities) throws Exception {
        if (userDTO == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        if (sessionDTO == null) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        @Nullable final List<Task> taskList = new ArrayList<>();
        if (entities == null) return true;
        for (TaskDTO taskDTO : entities) {
            @Nullable final Project project = projectService.findOneById(taskDTO.getUserId(), taskDTO.getProjectId());
            taskList.add(DtoConversionUtil.dtoToTask(taskDTO, project, user));
        }
        taskService.saveBin(taskList);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlJsonTask(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "tasks") @Nullable List<TaskDTO> entities) throws Exception {
        if (userDTO == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        if (sessionDTO == null) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        @Nullable final List<Task> taskList = new ArrayList<>();
        if (entities == null) return true;
        for (TaskDTO taskDTO : entities) {
            @Nullable final Project project = projectService.findOneById(taskDTO.getUserId(), taskDTO.getProjectId());
            taskList.add(DtoConversionUtil.dtoToTask(taskDTO, project, user));
        }
        taskService.saveFasterxmlJson(taskList);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlXmlTask(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "tasks") @Nullable List<TaskDTO> entities) throws Exception {
        if (userDTO == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        if (sessionDTO == null) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        @Nullable final List<Task> taskList = new ArrayList<>();
        if (entities == null) return true;
        for (TaskDTO taskDTO : entities) {
            @Nullable final Project project = projectService.findOneById(taskDTO.getUserId(), taskDTO.getProjectId());
            taskList.add(DtoConversionUtil.dtoToTask(taskDTO, project, user));
        }
        taskService.saveFasterxmlXml(taskList);
        return true;
    }

    @WebMethod
    public boolean saveJaxbJsonTask(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "tasks") @Nullable List<TaskDTO> entities) throws Exception {
        if (userDTO == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        if (sessionDTO == null) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        @Nullable final List<Task> taskList = new ArrayList<>();
        if (entities == null) return true;
        for (TaskDTO taskDTO : entities) {
            @Nullable final Project project = projectService.findOneById(taskDTO.getUserId(), taskDTO.getProjectId());
            taskList.add(DtoConversionUtil.dtoToTask(taskDTO, project, user));
        }
        taskService.saveJaxbJson(taskList);
        return true;
    }

    @WebMethod
    public boolean saveJaxbXmlTask(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "tasks") @Nullable List<TaskDTO> entities) throws Exception {
        if (userDTO == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        if (sessionDTO == null) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        @Nullable final List<Task> taskList = new ArrayList<>();
        if (entities == null) return true;
        for (TaskDTO taskDTO : entities) {
            @Nullable final Project project = projectService.findOneById(taskDTO.getUserId(), taskDTO.getProjectId());
            taskList.add(DtoConversionUtil.dtoToTask(taskDTO, project, user));
        }
        taskService.saveJaxbXml(taskList);
        return true;
    }


    @WebMethod
    public @Nullable List<TaskDTO> loadBinTask(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (userDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<Task> taskList =  taskService.loadBin();
        if (taskList == null) return null;
        @Nullable final List<TaskDTO> taskDtoList = new ArrayList<>();
        for (Task task : taskList) {
            taskDtoList.add(DtoConversionUtil.taskToDto(task));
        }
        return taskDtoList;
    }

    @WebMethod
    public @Nullable List<TaskDTO> loadFasterxmlJsonTask(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (userDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<Task> taskList =  taskService.loadFasterxmlJson();
        if (taskList == null) return null;
        @Nullable final List<TaskDTO> taskDtoList = new ArrayList<>();
        for (Task task : taskList) {
            taskDtoList.add(DtoConversionUtil.taskToDto(task));
        }
        return taskDtoList;
    }

    @WebMethod
    public @Nullable List<TaskDTO> loadFasterxmlXmlTask(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (userDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<Task> taskList =  taskService.loadFasterxmlXml();
        if (taskList == null) return null;
        @Nullable final List<TaskDTO> taskDtoList = new ArrayList<>();
        for (Task task : taskList) {
            taskDtoList.add(DtoConversionUtil.taskToDto(task));
        }
        return taskDtoList;
    }

    @WebMethod
    public @Nullable List<TaskDTO> loadJaxbJsonTask(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (userDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<Task> taskList =  taskService.loadJaxbJson();
        if (taskList == null) return null;
        @Nullable final List<TaskDTO> taskDtoList = new ArrayList<>();
        for (Task task : taskList) {
            taskDtoList.add(DtoConversionUtil.taskToDto(task));
        }
        return taskDtoList;
    }

    @WebMethod
    public @Nullable List<TaskDTO> loadJaxbXmlTask(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (userDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<Task> taskList =  taskService.loadJaxbXml();
        if (taskList == null) return null;
        @Nullable final List<TaskDTO> taskDtoList = new ArrayList<>();
        for (Task task : taskList) {
            taskDtoList.add(DtoConversionUtil.taskToDto(task));
        }
        return taskDtoList;
    }*/

}
