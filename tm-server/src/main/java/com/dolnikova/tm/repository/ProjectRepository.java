package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Setter
@Repository
@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> {

    private EntityManager entityManager;

    public ProjectRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = entityManager.find(Project.class, id);
        System.out.println("id: " + id + " user_id: " + userId);
        if (project == null) return null;
        if (userId.equals(project.getUser().getId())) return project;
        return null;
    }

    public Project findOneByName(@NotNull final String name) {
        return entityManager.
                createQuery("SELECT p from Project p where p.name = :name", Project.class).
                setParameter("name", name).
                getSingleResult();
    }

    public @Nullable List<Project> findAll(@NotNull final String userId) {
        @Nullable final List<Project> projects = entityManager.
                createQuery("SELECT p FROM Project p WHERE p.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
        return projects;
    }

    public @Nullable List<Project> findAllByName(@NotNull final String ownerId, @NotNull final String text) {
        @Nullable final List<Project> projects = entityManager.
                createQuery("SELECT p FROM Project p WHERE p.name LIKE :text", Project.class).
                setParameter("text", text).
                getResultList();
        return projects;
    }

    public @Nullable List<Project> findAllByDescription(@NotNull final String ownerId, @NotNull final String text) {
        @Nullable final List<Project> projects = entityManager.
                createQuery("SELECT p FROM Project p WHERE p.description LIKE :text", Project.class).
                setParameter("text", text).
                getResultList();
        return projects;
    }

    public void persist(@NotNull final Project project) {
        entityManager.persist(project);
    }

    public void merge(@NotNull final String newData,
                      @NotNull final Project dbProject,
                      @NotNull final DataType dataType) {
        if (dataType.equals(DataType.NAME)) {
            dbProject.setName(newData);
            entityManager.merge(dbProject);
        }
        if (dataType.equals(DataType.DESCRIPTION)) {
            dbProject.setDescription(newData);
            entityManager.merge(dbProject);
        }
    }

    public void remove(@NotNull final Project project) {
        System.out.println("!!! " + project.getId());
        entityManager.remove(entityManager.find(Project.class, project.getId()));
    }

    public void saveBin(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    public void saveFasterxmlJson(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    public void saveFasterxmlXml(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
        System.out.println(this.getClass().getName());
    }

    public void saveJaxbJson(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    public void saveJaxbXml(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    public List<Project> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<Project> projects = (ArrayList) objectInputStream.readObject();
            return projects;
        }
    }

    @Nullable
    public List<Project> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<Project> projects = objectMapper.readValue(file, ArrayList.class);
        return projects;
    }

    @Nullable
    public List<Project> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<Project> projects = xmlMapper.readValue(file, ArrayList.class);
        return projects;
    }

    @Nullable
    public List<Project> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Project> projects = (ArrayList<Project>) unmarshaller.unmarshal(file);
        return projects;
    }

    @Nullable
    public List<Project> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Project> projects = (ArrayList<Project>) unmarshaller.unmarshal(file);
        return projects;
    }

}
