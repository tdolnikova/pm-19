package com.dolnikova.tm.comparator;

import com.dolnikova.tm.endpoint.ProjectDTO;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

public class ProjectComparator {

    @Nullable
    public static Comparator<ProjectDTO> getStatusComparator() {
        return new Comparator<ProjectDTO>() {
            @Override
            public int compare(ProjectDTO o1, ProjectDTO o2) {
                return o1.getStatus().compareTo(o2.getStatus());
            }
        };
    }

    @Nullable
    public static Comparator<ProjectDTO> getCreationDateComparator() {
        return new Comparator<ProjectDTO>() {
            @Override
            public int compare(ProjectDTO o1, ProjectDTO o2) {
                return o1.getCreationDate().toGregorianCalendar().compareTo
                        (o2.getCreationDate().toGregorianCalendar());
            }
        };
    }

    @Nullable
    public static Comparator<ProjectDTO> getStartDateComparator() {
        return new Comparator<ProjectDTO>() {
            @Override
            public int compare(ProjectDTO o1, ProjectDTO o2) {
                return o1.getDateBegin().toGregorianCalendar().compareTo
                        (o2.getDateBegin().toGregorianCalendar());
            }
        };
    }

    @Nullable
    public static Comparator<ProjectDTO> getEndDateComparator() {
        return new Comparator<ProjectDTO>() {
            @Override
            public int compare(ProjectDTO o1, ProjectDTO o2) {
                return o1.getDateEnd().toGregorianCalendar().compareTo
                        (o2.getDateEnd().toGregorianCalendar());
            }
        };
    }

}
