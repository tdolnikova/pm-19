package com.dolnikova.tm.api.spring;

import com.dolnikova.tm.entity.Task;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface TaskSpringRepository extends JpaRepository<Task, String> {

    Task findByIdAndUserId(@Nullable String id, @Nullable String userId);

    Task findOneByNameAndUserId(@Nullable String name, @Nullable String userId);

    Collection<Task> findAllByUserId(@Nullable String userId);

    Collection<Task> findAllByNameAndUserId(@Nullable String name, @Nullable String userId);

    Collection<Task> findAllByDescriptionAndUserId(@Nullable String description, @Nullable String userId);

    void deleteAllByUserId(@Nullable String userId);

}
