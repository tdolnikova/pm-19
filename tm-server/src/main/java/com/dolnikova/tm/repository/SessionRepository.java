package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.ISessionRepository;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Session;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Repository
@NoArgsConstructor
public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    private EntityManager entityManager;

    public SessionRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull String userId, @NotNull String id) {
        @Nullable final Session session = entityManager.find(Session.class, id);
        if (session == null) return null;
        if (userId.equals(session.getUser().getId()))
            return session;
        return null;
    }

    @Nullable
    @Override
    public Session findOneByUserId(@NotNull final String userId) {
        return entityManager.
                createQuery("SELECT s FROM Session s WHERE s.user.id = :userId", Session.class).
                setParameter("userId", userId).
                getSingleResult();
    }

    @Nullable
    @Override
    public Session findOneBySignature(@NotNull final String signature) {
        return entityManager.
                createQuery("SELECT s FROM Session s WHERE s.signature = :signature", Session.class).
                setParameter("signature", signature).
                getSingleResult();
    }

    @Override
    public @Nullable List<Session> findAll(@NotNull final String userId) {
        @Nullable final List<Session> sessions = entityManager.
                createQuery("SELECT s FROM Session s WHERE s.user.id = :userId", Session.class).
                setParameter("userId", userId).
                getResultList();
        return sessions;
    }

    @Override
    public void persist(@NotNull final Session session) {
        entityManager.persist(session);
    }

    @Override
    public void remove(@NotNull final Session dbSession) {
        entityManager.remove(dbSession);
    }

    @Override
    public void saveBin(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveJaxbJson(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<Session> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<Session> sessions = (ArrayList) objectInputStream.readObject();
            return sessions;
        }
    }

    @Nullable
    @Override
    public List<Session> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<Session> sessions = objectMapper.readValue(file, ArrayList.class);
        return sessions;
    }

    @Nullable
    @Override
    public List<Session> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<Session> sessions = xmlMapper.readValue(file, ArrayList.class);
        return sessions;
    }

    @Nullable
    @Override
    public List<Session> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_JSON);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Session> sessions = (ArrayList<Session>) unmarshaller.unmarshal(file);
        return sessions;
    }

    @Nullable
    @Override
    public List<Session> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_XML);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Session> sessions = (ArrayList<Session>) unmarshaller.unmarshal(file);
        return sessions;
    }

    /*@Nullable
    private Session fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString(DbConstant.ID));
        session.setUserId(row.getString(DbConstant.USER_ID));
        session.setSignature(row.getString(DbConstant.SIGNATURE));
        return session;
    }*/

}
