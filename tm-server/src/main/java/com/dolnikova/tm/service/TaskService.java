package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.api.spring.TaskSpringRepository;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.logging.Logger;

@Service
@Transactional
@NoArgsConstructor
public class TaskService extends AbstractService<Task> implements ITaskService {

    @Autowired
    private TaskSpringRepository taskRepository;
    private final Logger LOGGER = Logger.getLogger(TaskService.class.getName());

    @Nullable
    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        LOGGER.info("[Поиск задачи по id]");
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findByIdAndUserId(id, userId);
    }

    @Override
    public Task findOneByName(@Nullable final String name, @Nullable final String userId) {
        LOGGER.info("[Поиск задачи по названию]");
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findOneByNameAndUserId(name, userId);
    }

    @Override
    public @Nullable Collection<Task> findAll(@Nullable final String userId) {
        LOGGER.info("[Поиск всех задач]");
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public @Nullable Collection<Task> findAllByName(@Nullable final String userId, @Nullable final String name) {
        LOGGER.info("[Поиск всех задач по названию]");
        if (userId == null || userId.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findAllByNameAndUserId(name, userId);
    }

    @Override
    public @Nullable Collection<Task> findAllByDescription(@Nullable final String userId, @Nullable final String description) {
        LOGGER.info("[Поиск всех задач по описанию]");
        if (userId == null || userId.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.findAllByDescriptionAndUserId(description, userId);
    }

    @Override
    public void persist(@Nullable final Task task) {
        LOGGER.info("[Вставка задачи]");
        if (task == null) return;
        taskRepository.save(task);
    }

    @Override
    public void persistList(@Nullable Collection<Task> list) {
        if (list == null || list.isEmpty()) return;
        taskRepository.saveAll(list);
    }

    @Override
    public void merge(@Nullable final String newData,
                      @Nullable final Task task,
                      @Nullable final DataType dataType) {
        LOGGER.info("[Изменение данных задачи]");
        if (newData == null || newData.isEmpty()) return;
        if (task == null || dataType == null) return;
        if (dataType.equals(DataType.NAME)) {
            task.setName(newData);
            taskRepository.save(task);
        }
        if (dataType.equals(DataType.DESCRIPTION)) {
            task.setDescription(newData);
            taskRepository.save(task);
        }
    }

    @Override
    public void remove(@Nullable final Task task) {
        if (task == null) return;
        LOGGER.info("[Удаление задачи " + task.getId() +"]");
        taskRepository.delete(task);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        LOGGER.info("[Удаление всех задач пользователя]");
        if (userId == null || userId.isEmpty()) return;
        taskRepository.deleteAllByUserId(userId);
    }

}
