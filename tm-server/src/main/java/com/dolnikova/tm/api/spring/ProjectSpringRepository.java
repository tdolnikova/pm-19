package com.dolnikova.tm.api.spring;

import com.dolnikova.tm.entity.Project;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface ProjectSpringRepository extends JpaRepository<Project, String> {

    Project findProjectByIdAndUserId(@Nullable String id, @Nullable String userId);

    Project findProjectByName(@Nullable String name);

    Collection<Project> findAllByUserId(@Nullable String userId);

    Collection<Project> findAllByUserIdAndNameContaining(@Nullable String userId, @Nullable String text);

    Collection<Project> findAllByUserIdAndDescriptionContaining(@Nullable String userId, @Nullable String text);

    void deleteAllByUserId(@Nullable String userId);

}
