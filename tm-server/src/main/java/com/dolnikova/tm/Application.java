package com.dolnikova.tm;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.configuration.AppConfig;
import com.dolnikova.tm.endpoint.UserEndpoint;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public final class Application {

    public static void main(String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean("bootstrap", Bootstrap.class);
        bootstrap.init();
    }

}