package com.dolnikova.tm.enumerated;

public enum Status {

    PLANNED("ЗАПЛАНИРОВАНО"),
    IN_PROCESS("В ПРОЦЕССЕ"),
    READY("ГОТОВО");

    private final String name;

    Status(final String name) {
        this.name = name;
    }

    public String displayName() {
        return name;
    }

}
