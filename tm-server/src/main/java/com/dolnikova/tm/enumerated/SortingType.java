package com.dolnikova.tm.enumerated;

public enum SortingType {

    BY_CREATION_DATE("by creation date"),
    BY_START_DATE("by start date"),
    BY_END_DATE("by end date"),
    BY_STATUS("by status");

    private final String name;

    SortingType(final String name) {
        this.name = name;
    }

    public String displayName() {
        return name;
    }

}
