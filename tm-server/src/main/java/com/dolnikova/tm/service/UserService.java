package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.api.spring.UserSpringRepository;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.util.PasswordHashUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

@Service
@Transactional
@NoArgsConstructor
public class UserService extends AbstractService<User> implements IUserService {

    @Autowired
    private UserSpringRepository userRepository;
    private final Logger LOGGER = Logger.getLogger(UserService.class.getName());

    @Nullable
    public User findOneById(@Nullable final String id) {
        LOGGER.info("[Поиск пользователя по id: [ " + id + " ]");
        if (id == null || id.isEmpty()) return null;
        return userRepository.findUserById(id);
    }

    @Override
    public User findOneByLogin(@Nullable final String login) {
        LOGGER.info("[Поиск пользователя по логину: [ " + login + " ]");
        if (login == null || login.isEmpty()) return null;
        return userRepository.findUserByLogin(login);
    }

    @Override
    public User findOneBySession(@Nullable Session session) {
        if (session == null || session.getUser() == null) return null;
        LOGGER.info("[Поиск пользователя по сессии: [ " + session.getId() + " ]");
        @Nullable final String userId = session.getUser().getId();
        return userRepository.findUserById(userId);
    }

    @Override
    public @Nullable List<User> findAll() {
        LOGGER.info("[Поиск всех пользователей]");
        return userRepository.findAll();
    }

    @Override
    public @Nullable Collection<User> findAllByLogin(@Nullable final String ownerId, @Nullable final String login) {
        LOGGER.info("[Поиск всех пользователей по логину]");
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (login == null || login.isEmpty()) return null;
        return userRepository.findAllByLogin(login);
    }

    @Override
    public void persist(@Nullable final User user) {
        LOGGER.info("[Вставка пользователя]");
        if (user == null) return;
        @Nullable final String passwordHash = PasswordHashUtil.md5(user.getPasswordHash());
        if (passwordHash == null || passwordHash.isEmpty()) return;
        user.setPasswordHash(passwordHash);
        userRepository.save(user);
    }

    @Override
    public void persistList(@Nullable Collection<User> list) {
        if (list == null || list.isEmpty()) return;
        for (User user : list) persist(user);
        userRepository.saveAll(list);
    }

    @Override
    public void merge(@Nullable final String newData,
                      @Nullable final User user,
                      @Nullable final DataType dataType) {
        LOGGER.info("[Изменение данных пользователя]");
        if (newData == null || newData.isEmpty()) return;
        if (user == null || dataType == null) return;
        if (dataType.equals(DataType.LOGIN)) {
            user.setLogin(newData);
            userRepository.save(user);
        }
        if (dataType.equals(DataType.ROLE)) {
            user.setRole(Role.valueOf(newData));
            userRepository.save(user);
        }
        if (dataType.equals(DataType.PASSWORD)) {
            user.setPasswordHash(newData);
            userRepository.save(user);
        }

    }

    @Override
    public void remove(@Nullable final User user) {
        LOGGER.info("[Удаление пользователя]");
        if (user == null) return;
        userRepository.delete(user);
    }

    @Override
    public void removeAll() {
        LOGGER.info("[Удаление всех пользователей]");
        userRepository.deleteAll();
    }

    @Override
    public boolean checkPassword(@Nullable String userId, @Nullable String userInput) {
        if (userId == null || userId.isEmpty()) return false;
        if (userInput == null || userInput.isEmpty()) return false;
        @Nullable final String hashedInput = PasswordHashUtil.md5(userInput);
        if (hashedInput == null || hashedInput.isEmpty()) return false;
        // !!! !!! !!! /// !!! !!! !!! /// !!! !!! !!! /// !!! !!! !!! ///
        @Nullable final User user = findOneById(userId);
        if (user != null) {
            @Nullable final String password = user.getPasswordHash();
            return hashedInput.equals(password);
        }
        return false;
    }

}
