package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface IProjectService extends IService<Project> {

    @Nullable Collection<Project> findAll(@Nullable final User user);

    @Nullable
    Project findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Project findOneByName(final @Nullable String name);

    @Nullable
    Collection<Project> findAll();

    @Nullable Collection<Project> findAllByName(@Nullable String ownerId, @Nullable String text);

    @Nullable Collection<Project> findAllByDescription(@Nullable String ownerId, @Nullable String text);

    @Override
    void persist(@Nullable final Project entity);

    @Override
    void persistList(final @Nullable Collection<Project> list);

    void merge(@Nullable final String newData, @Nullable final Project entityToMerge, @Nullable final DataType dataType);

    @Override
    void remove(@Nullable final Project entity);

    void removeAll(@Nullable final String userId);

}
