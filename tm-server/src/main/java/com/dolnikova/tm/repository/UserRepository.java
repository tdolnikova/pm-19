package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.util.PasswordHashUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Setter
@Repository
@NoArgsConstructor
public final class UserRepository implements IUserRepository {

    private EntityManager entityManager;

    public UserRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public List<User> findAll(Sort sort) {
        return null;
    }

    @Override
    public List<User> findAllById(Iterable<String> iterable) {
        return null;
    }

    @Override
    public <S extends User> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends User> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<User> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public User getOne(String s) {
        return null;
    }

    @Override
    public <S extends User> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends User> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends User> S save(S s) {
        return null;
    }

    @Override
    public Optional<User> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(User user) {

    }

    @Override
    public void deleteAll(Iterable<? extends User> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends User> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends User> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends User> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends User> boolean exists(Example<S> example) {
        return false;
    }

    /*
     *
     * */
    @Nullable
    public User findOneById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        return entityManager.
                createQuery("SELECT u FROM User u WHERE u.login = :login", User.class).
                setParameter("login", login).
                getSingleResult();
    }

    @Nullable
    @Override
    public List<User> findAll(@NotNull final String userId) {
//        @Nullable final List<User> users = entityManager.
//                createQuery("SELECT u FROM User u", User.class).getResultList();
        @Nullable final List<User> users = findAll();
        for (User user : users) {

        }
        return users;
    }

    @Nullable
    @Override
    public List<User> findAllByLogin(@NotNull final String ownerId, @NotNull final String login) {
        @Nullable final List<User> users = entityManager.
                createQuery("SELECT u FROM User u WHERE u.login LIKE :login", User.class).
                setParameter("login", login).
                getResultList();
        return users;
    }

    @Override
    public void persist(@NotNull final User user) {
        @Nullable final String password = user.getPasswordHash();
        if (password == null) return;
        user.setPasswordHash(PasswordHashUtil.md5(password));
        entityManager.persist(user);
    }

    @Override
    public void merge(@NotNull final String newData,
                      @NotNull final User dbUser,
                      @NotNull final DataType dataType) {
        if (dataType.equals(DataType.LOGIN)) {
            dbUser.setLogin(newData);
            entityManager.merge(dbUser);
        }
        if (dataType.equals(DataType.ROLE)) {
            dbUser.setRole(Role.valueOf(newData));
            entityManager.merge(dbUser);
        }
        if (dataType.equals(DataType.PASSWORD)) {
            dbUser.setPasswordHash(newData);
            entityManager.merge(dbUser);
        }
    }

    @Override
    public void remove(@NotNull final User dbUser) {
        entityManager.remove(dbUser);
    }

    @Override
    public void saveBin(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveJaxbJson(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<User> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_BIN);
        try (@NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<User> users = (ArrayList) objectInputStream.readObject();
            return users;
        }
    }

    @Nullable
    @Override
    public List<User> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<User> users = objectMapper.readValue(file, ArrayList.class);
        return users;
    }

    @Nullable
    @Override
    public List<User> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<User> users = xmlMapper.readValue(file, ArrayList.class);
        return users;
    }

    @Nullable
    @Override
    public List<User> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_JSON);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<User> users = (ArrayList<User>) unmarshaller.unmarshal(file);
        return users;
    }

    @Nullable
    @Override
    public List<User> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<User> users = (ArrayList<User>) unmarshaller.unmarshal(file);
        return users;
    }

}
