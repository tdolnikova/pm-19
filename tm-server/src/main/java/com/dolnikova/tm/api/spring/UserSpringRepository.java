package com.dolnikova.tm.api.spring;

import com.dolnikova.tm.entity.User;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface UserSpringRepository extends JpaRepository<User, String> {

    User findUserByLogin(@Nullable String login);

    User findUserById(@Nullable String id);

    Collection<User> findAllByLogin(@Nullable String login);

}
